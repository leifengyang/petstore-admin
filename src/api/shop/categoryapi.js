import http from "@/utils/http"

//查询商城分类树数据

const  getShopCategoryTreeAPI = ()=>{
    return  http.get(`/api/shop/category/tree`)
}

export {getShopCategoryTreeAPI}
