import http from "@/utils/http.js"



//获取菜单树
const getMenuTree = ()=>{
    console.log("menu-tree....")
    return http.get(`/api/sys/menu/tree`)
}


//获取某个菜单值
const getMenuById = (id)=>{
    return http.get(`/api/sys/menu/${id}`)
}


//更新某个菜单
const  updateMenuAPI = (menu) => {
    return http.put(`/api/sys/menu`,menu)
}

//新增菜单
const  addMenuAPI = (data)=>{
    return http.post(`/api/sys/menu`,data)
}

//删除菜单
const deleteMenuByIdAPI = (id) =>{
    return http.delete(`/api/sys/menu/${id}`)
}

export  {getMenuTree,getMenuById,updateMenuAPI,deleteMenuByIdAPI,addMenuAPI}
