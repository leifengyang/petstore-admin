import http from "@/utils/http"


//用户的所有功能都写在这里

//分页获取用户数据
const  getUserPageData =  (pn,param)=>{
   //本身是一个Promise
   return  http.get(`/api/sys/user/page/${pn}`,{
      params: param
   })
}


//添加用户API
const addUserDataAPI = (data)=>{
   return http.post(`/api/sys/user`,data);
}


//按照id删除用户
const deleteUserAPI = (id)=>{
   return http.delete(`/api/sys/user/${id}`);
}

//修改用户
const  updateUserAPI = (data)=>{
   return http.put("/api/sys/user",data);
}


//修改头像
const updateUserHeaderImgAPI = (data)=>{
   return http.put("/api/sys/user/headerimg",data);
}


export  {getUserPageData,addUserDataAPI,deleteUserAPI,updateUserAPI,updateUserHeaderImgAPI}
