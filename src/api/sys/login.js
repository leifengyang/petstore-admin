import http from "@/utils/http"


//登录
const loginAPI = (data)=>{
    return  http.post(`/api/sys/user/login`,data)
}


//检查令牌
const checkTokenAPI = async ()=>{
    return await http.get("/api/sys/user/token/check");
}


//退出
const logoutAPI = ()=>{
    return  http.post(`/api/sys/user/logout`)
}

export {loginAPI,checkTokenAPI,logoutAPI}
