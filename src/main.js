// 样式初始化
import 'normalize.css'

import { createApp } from 'vue'
import { createPinia } from 'pinia'
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'

import App from './App.vue'
import router from './router'

import eIconPicker from 'e-icon-picker';
import "e-icon-picker/icon/default-icon/symbol.js"; //基本彩色图标库
import 'e-icon-picker/index.css'; // 基本样式，包含基本图标
import 'font-awesome/css/font-awesome.min.css';

import * as ElementPlusIconsVue from '@element-plus/icons-vue'; //element-plus 图标库
import eIconList from 'e-icon-picker/icon/default-icon/eIconList.js'
import fontAwesomeList from "e-icon-picker/icon/fontawesome/font-awesome.v4.7.0.js"

//推荐图标选择器：e-icon-picker

const app = createApp(App)

// 全局注册所以element-plus图标
for (const [key, component] of Object.entries(ElementPlusIconsVue)) {
    app.component(key, component)
}
app.use(eIconPicker, {
    addIconList: [...fontAwesomeList],//全局添加图标
    removeIconList: [],//全局删除图标
    zIndex: 3100//选择器弹层的最低层,全局配置
});

app.use(createPinia())
app.use(router)
app.use(ElementPlus)

app.mount('#app')
