import { v4 as uuidv4 } from 'uuid';

const initDevice = ()=>{
    let deviceId = localStorage.getItem("deviceId");
    if(deviceId == null || deviceId === "" || deviceId == undefined){
        localStorage.setItem("deviceId",uuidv4());
    }
}

const getDeviceId = ()=>{
    let deviceId = localStorage.getItem("deviceId");
    if(deviceId == null || deviceId === "" || deviceId == undefined){
        //重新初始化
        initDevice();
        //重新获取数据
        deviceId = localStorage.getItem("deviceId");
    }
    return deviceId;
}

export {initDevice,getDeviceId}
