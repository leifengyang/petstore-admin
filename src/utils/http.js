import axios from "axios"
import {getUserToken} from "@/utils/usertoken"




const http = axios.create({
    baseURL: 'http://localhost',  //基础路径, 网关地址
    timeout: 1000
});


// 添加请求拦截器； 每次请求放令牌
http.interceptors.request.use(function (config) {
    // console.log("拦截器工作中》。。",getUserToken())
    //发请求带头
    config.headers['token'] = getUserToken();
    // 在发送请求之前做些什么
    return config;
}, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
});


//响应拦截器
// 添加响应拦截器
http.interceptors.response.use(function (response) {
    // 2xx 范围内的状态码都会触发该函数。
    // 对响应数据做点什么

    return response;

}, function (error) {
    // 超出 2xx 范围的状态码都会触发该函数。
    // 对响应错误做点什么
    if (error.response.status === 403) {
        //业务规定，所有的403必须重新登录
        location.href = "/login";
    }
    return Promise.reject(error);
});

//   怎么导出，怎么导入
export default http
